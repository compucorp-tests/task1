# Task 1: Javascript

## Description
Build a simple HTML/JS app preferably based on AngularJS v1 which, when you open the site, asks for a permission to read your current location, and then uses the http://openweathermap.org/current public API to fetch the weather for that location and displays it.
If the user denies to share the location, show a little input form for entering postcode or country, and display the weather based on that location.

## Instructions
The main focus of this activity is to access your JavaScript skills. Therefore, style the app appropriately using your own judgement.
Upload the completed project to your Github account, and send us a link.
If you are also able to host the solution simply then please do. We recommend using AWS free tier but reasonable hosting costs will be reimbursed.

## Extra brownie points!
Use object-oriented JavaScript to organise your code (hint: use classes, inheritance, etc.).
Structure your code to be clean, efficient and maintainable.
Write tests for your application.
Use of design patterns would be a plus.
Anything else that could make us say wow!

### Live
https://task-01.herokuapp.com/

### Technologies used

* Grunt
* Bower
* LESS
* HTML5
* CSS3
* AngularJS v1
* Node
* Express
* Bootstrap

### To run local
It’s A Piece Of :cake:


Just clone: `git clone git@gitlab.com:compucorp-tests/task1.git`

Enter the project folder: `cd task1`

Install the dev dependencies: `npm install`

Run grunt local: `grunt dev`
