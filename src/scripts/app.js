var app = angular.module('weatherApp', []);

(function() {
  'use strict';

  app.controller('MainController', ['$scope', '$http', '$sce', '$log', function($scope, $http, $sce, $log) {

      var main = this;
      var scope = $scope;

      scope.my = { locationEnable : true };
      scope.city = "";
      scope.country = "";
      scope.lat = '';
      scope.lon = '';
      scope.icon = '';
      scope.date = new Date();
      scope.temp = '';
      scope.temp_max = '';
      scope.temp_min = '';

      var url = '';
      var url_jsonp = 'https://jsonp.afeld.me/?url=';
      var key = '7252df49ff5a0c1ca55c5211c0c98308';

      var round = function(num) {
        return parseInt(Math.round(num * 10) / 10) || '';
      };

      var prepareUri = function (uri) {
        return uri.replace(/&/g, '%26');
      };

      main.convertKelvinToCelcius = function(kelvin) {
        return round(kelvin - 273.15);
      };

      main.getLocation = function() {
        navigator.geolocation.getCurrentPosition(success, error);

        function success(position) {

          scope.lat = position.coords.latitude;
          scope.lon = position.coords.longitude;

          url = prepareUri(url_jsonp + 'http://api.openweathermap.org/data/2.5/weather?lat=' + round(scope.lat) + '&lon=' + round(scope.lon) + '&APPID=' + key + '&callback=JSON_CALLBACK');

          $http.jsonp(url, {cache: true}).success(function(data) {
              scope.country = data.sys.country;
              scope.humidity = data.main.humidity;
              scope.temp = main.convertKelvinToCelcius(data.main.temp);
              scope.temp_max = main.convertKelvinToCelcius(data.main.temp_max);
              scope.temp_min = main.convertKelvinToCelcius(data.main.temp_min);
              scope.wind = data.wind.speed;
              scope.icon = data.weather.icon;
              scope.city = data.name;
              scope.date = new Date();

            })
            .error(function(err) {
              $log.error("$http error: ", err);
            });
        }

        function error(e) {
          if(e.code === 1){
            scope.my.locationEnable = false;
          }
          $log.error("navigator.geolocation error: ", e);
        }
      };

      main.getLocationByCity = function(city) {
        main.lat = '';

        url = prepareUri(url_jsonp + 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&APPID=' + key + '&callback=JSON_CALLBACK');
        $http.jsonp(url, {cache: true}).success(function(data) {
            scope.country = data.sys.country;
            scope.humidity = data.main.humidity;
            scope.temp = main.convertKelvinToCelcius(data.main.temp);
            scope.temp_max = main.convertKelvinToCelcius(data.main.temp_max);
            scope.temp_min = main.convertKelvinToCelcius(data.main.temp_min);
            scope.wind = data.wind.speed;
            scope.icon = data.weather.icon;
            scope.city = data.name;
            scope.post.city = "";
            scope.date = new Date();

          })
          .error(function(err) {
            $log.error("$http error: ", err);
          });
      };

      main.getLocationByPostcode = function(postcode) {
        main.lat = '';

        url = prepareUri(url_jsonp + 'http://api.openweathermap.org/data/2.5/weather?zip=' + postcode+ ",uk" + '&APPID=' + key + '&callback=JSON_CALLBACK');
        $http.jsonp(url, {cache: true}).success(function(data) {
            scope.country = data.sys.country;
            scope.humidity = data.main.humidity;
            scope.temp = main.convertKelvinToCelcius(data.main.temp);
            scope.temp_max = main.convertKelvinToCelcius(data.main.temp_max);
            scope.temp_min = main.convertKelvinToCelcius(data.main.temp_min);
            scope.wind = data.wind.speed;
            scope.icon = data.weather.icon;
            scope.city = data.name;
            scope.post.postcode = "";
            scope.date = new Date();

          })
          .error(function(err) {
            $log.error("$http error: ", err);
          });
      };
    }]);
})();
